# 👋 I'm Kunal Kumar

#### I'm an Aspiring Full Stack Web Developer From India

![](https://komarev.com/ghpvc/?username=Kunal2007-web)

## About Me
👦 I am a 14 Year Old Boy who likes to code. I'm also a Linux User and FOSS Enthusiast.\
👨‍💻 I am Currently Learning the Basics of Web Development (HTML, CSS, Javascript) and Designing/Design Principles.

## Projects

### Completed
These are some of my Personal Projects.

Websites:
1. [Loruki](https://kunal-loruki.netlify.app)  	
2. [Hulu Landing Clone](https://hulu-landing-clone.netlify.app)
3. [Creative Web Dev](https://creative-web-dev.pages.dev)

### Working On
These are the Projects that I'm Working on.

Websites:
1. Coffee Junkie (Designing)

Connect to me through: [Instagram](https://www.instagram.com/kunalkumarchourasiya)  [Twitter](https://twitter.com/kunalku2007) [Github](https://github.com/Kunal2007-web)\
                       Email: kunalkumarchourasiya79@gmail.com , kunalkumarchourasiya2021@gmail.com 
